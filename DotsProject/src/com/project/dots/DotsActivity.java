package com.project.dots;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.project.dots.R;


public class DotsActivity extends ActionBarActivity {
	MyView vi;
	public String localUserName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       Intent i = getIntent();
       setContentView(R.layout.activity_dots);
       vi = (MyView)findViewById(R.id.draw);
       boolean[] onDeviceAndPlay = new boolean[2];
       onDeviceAndPlay = i.getBooleanArrayExtra("onDeviceAndPlay");
       vi.setGameType(onDeviceAndPlay);
       vi.setActivity(this);
       TextView title = (TextView)findViewById(R.id.title);
       TextView lime = (TextView)findViewById(R.id.lime);
       TextView lemon = (TextView)findViewById(R.id.lemon);
       vi.setScoreViews(lime,lemon);        
       //for testing op
       /*
       final EditText x = (EditText)findViewById(R.id.xVal);
       final EditText y = (EditText)findViewById(R.id.yVal);
       
       Button b = (Button)findViewById(R.id.opMoves);
       b.setOnClickListener(new OnClickListener(){

		@Override
		public void onClick(View arg0) {	
			vi.loadMoves(" 1 7 7 13 13 19 19 25 25 31 ");
		}
    	   
       });
       */
       
	   Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Font.ttf");
		
	   title.setTypeface(custom_font); 
	   lime.setTypeface(custom_font);
	   lemon.setTypeface(custom_font);
       
    }
    
    public void updateGameObject(String m) {
    	System.out.println("CALLED BACK GAME HISTORY: " + m);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       getMenuInflater().inflate(R.menu.main, menu);
       return true;
   }

	public void onGameOver(int u1Score, int u2Score) {
		//lulz
	}
	
	public void onRematch() {
		//lulz
	}
}
