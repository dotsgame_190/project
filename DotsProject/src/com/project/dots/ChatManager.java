package com.project.dots;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
/**
 * Handles reading and writing of messages with socket buffers. Uses a Handler
 * to post messages to UI thread for UI updates.
 */
public class ChatManager implements Runnable {
    private Socket socket = null;
    private Handler handler;
    String points = MyView.myMoves;
    public ChatManager(Socket socket, Handler handler) {
        this.socket = socket;
        this.handler = handler;
    }
    private static final String TAG = "Moves";
    @Override
    public void run() 
    {
            byte[] buffer = new byte[1024];
            Message msg = Message.obtain();
            msg.obj = points;
            handler.sendMessage(msg);
            
            while (true) {
                    if(MyView.onDevice == true)
                    {
                    	if(points != "")
                    	{
                    		handler.obtainMessage().sendToTarget();
                    		MyView.onDevice = false;
                    	}
                    }
                try {
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
    }
    public void write(String index) {
    	points = index;
    }
}
