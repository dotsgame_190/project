package com.project.dots;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.project.dots.Constants;
import com.project.dots.R;

public class GamePage extends ActionBarActivity 
{
	
	String username = "Guest";
	int dots = 1;
	int tictac = 2;
	int connect = 3;
	int local = 1;
	int bluewi = 2;
	int multi = 3;
	ImageView localView;
	ImageView wifiView;
	ImageView multiView;
	TextView pick;
	TextView localText;
	TextView wifiText;
	TextView multiText;

	static int game;
	static int method;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_page);
		localView = (ImageView)findViewById(R.id.local);
		wifiView = (ImageView)findViewById(R.id.wifi);
		multiView = (ImageView)findViewById(R.id.multi);	
		localText = (TextView)findViewById(R.id.localText);
		wifiText = (TextView)findViewById(R.id.wifiText);
		multiText = (TextView)findViewById(R.id.multiText);
		pick = (TextView)findViewById(R.id.textView1);
		
		username = getIntent().getStringExtra(Constants.IntentUserName);

		
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Font.ttf");
		
		localText.setTypeface(custom_font); 
		wifiText.setTypeface(custom_font); 
		multiText.setTypeface(custom_font); 
		pick.setTypeface(custom_font); 
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
	}
	
	
	public void chooseGamePlay(View v)
	{
	    switch (v.getId()) 
	    {
        case R.id.local:
            	method = local;
            	localText.setTextColor(Color.YELLOW);
            	wifiText.setTextColor(Color.WHITE);
            	multiText.setTextColor(Color.WHITE);
            break;
        case R.id.wifi:
            	method = bluewi;
            	localText.setTextColor(Color.WHITE);
            	wifiText.setTextColor(Color.YELLOW);
            	multiText.setTextColor(Color.WHITE);
            break;
        case R.id.multi:
        		method = multi;
        		localText.setTextColor(Color.WHITE);
            	wifiText.setTextColor(Color.WHITE);
            	multiText.setTextColor(Color.YELLOW);
        		
        	break;
	    }
	}
	
	public void showGame(View view) {
	    Intent intent;
	    System.out.println("method:" + method + "game:" + game);
	    if(method == bluewi)
	    {
	    	intent = new Intent(getApplicationContext(), WIFIDirectActivity.class);
	    	intent.putExtra(Constants.IntentUserName, username);
		    startActivity(intent);
	    }
	    else if (method == multi)
	    {
	    	intent = new Intent(getApplicationContext(), UserHomeActivity.class);
	    	intent.putExtra(Constants.IntentUserName, username);
	    	startActivity(intent);
	    }
	    else
	    {
	    	intent = new Intent(getApplicationContext(), DotsActivity.class);
		    //intent.putExtra(android.content.Intent.EXTRA_TEXT, method);
		    //intent.putExtra("method", method);
	    	/* For onDevice just set 1st arg to true and the second one to true, and 
	    	 * for onLine play set 1st arg to false and set 2nd arg to true for player 1,
	    	 * and false for player 2.
	    	 */
	    	boolean[] onDeviceAndPlay = {true,true};
	    	intent.putExtra("onDeviceAndPlay", onDeviceAndPlay);
		    startActivity(intent);
	    }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_page, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_game_page,
					container, false);
			return rootView;
		}
	}

}
