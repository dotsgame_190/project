package com.project.dots;

import java.util.ArrayList;
import java.util.Observable;

/* Handles the logic and internal representation of Dots game */
public class LineManager extends Observable {
	private int padding;
	final static int DOT_DIMENSION = 6;
	final static int SQUARE_DIMENSION = DOT_DIMENSION - 1; // 2dots/square
	private int separation; //distance between dots

	int dot1,dot2;
	GridSquare[] grid = new GridSquare[SQUARE_DIMENSION*SQUARE_DIMENSION];
	
	public LineManager() {
		assignEdges();
	}
	
	public void startNewGame() {
		assignEdges();
	}
	public void setPadding (int p) {
		padding = p;
	}
	
	public void setSeparation(int s) {
		separation = s;
	}
	
	public void evalCoords(float x0, float y0, float x, float y) {
		int dot1x = findDotIndex(x0);
		int dot1y = findDotIndex(y0);
		int dot2x = findDotIndex(x);
		int dot2y = findDotIndex(y);
		if (dot1x >= 0 && dot1y >= 0) { // checks for valid dot indices
			dot1 = dot1x + DOT_DIMENSION*dot1y;
			if (dot2x >= 0 && dot2y >= 0) {
				dot2 = dot2x + DOT_DIMENSION*dot2y;
				checkAndFind(dot1,dot2); // find the edge and check if marked
			}
		}
	}
	
	public float findNearestDot(float f) {
		int dotIndex = -1;
		for (int i = 0; i < 5; i++) {
			if (f < (padding + separation*(i + 0.5))) {
				dotIndex = i;
				break;
			}
		}
		if (dotIndex == -1)
			dotIndex = 5;
		return padding + dotIndex*separation;
	}
	
	public boolean checkAndFind(int x, int y) {
		dot1 = x;
		dot2 = y;
		return checkEdge(findEdge(x,y));
	}
		
	public boolean checkEdge(int[] sqAndE) {
		int square = sqAndE[0];
		int edge = sqAndE[1];
		if (edge == -1) // non valid edge
			return false;
		else if (grid[square].edges[edge].isMarked()) //marked edge 
			return false;
		else {
			int[] sqs = new int[2];
			int[] results = new int[]{0,0,0,0};
			grid[square].edges[edge].markDone();
			sqs = checkSquare(square); //check square and adjacent squares if complete
			//results holds drawing instructions for listener object
			results[0] = dot1;
			results[1] = dot2;
			results[2] = sqs[0];
			results[3] = sqs[1];
			setChanged();
			notifyObservers(results);
			return true;
		}
	}

	private int[] checkSquare(int s) {
		int[] squaresToColor = new int[]{-1,-1}; //at most 2
		ArrayList<Integer> l = new ArrayList<Integer>();
		l.add(s);
		if (s - 5 >= 0)  // square above
			l.add(s-5); 
		if (s - 1 >= 0) // square left
			l.add(s-1);
		if (s + 5 < grid.length) // square below
			l.add(s+5);
		if (s + 1 < grid.length) // square right
			l.add(s+1);
		for (int i = 0; i < l.size(); i++) {
			if (grid[l.get(i)].isComplete() && !grid[l.get(i)].isColored()) {
				if (squaresToColor[0] != -1)
					squaresToColor[1] = l.get(i);
				else
					squaresToColor[0] = l.get(i);
				grid[l.get(i)].setColored();
			}
		}
		return squaresToColor;
	}
		
	// hard-coded to specific canvas and sensitivity
	// 30 pixel range
	private int findDotIndex(float loc) {
		for (int i = 0; i < DOT_DIMENSION; i++) {
			float rightBound = padding + separation*i + 50;
			float leftBound = padding + separation*i - 50;
			if ( loc <= rightBound && loc >= leftBound )
				return i;
		}
		return -1;
	}

	// finds if there exists a valid edge between to dots
	// if difference == 1, horizontal edge
	// if difference == 6, vertical edge
	private int[] findEdge(int dot1, int dot2) {
		int fromDot = Math.min(dot1,dot2);
		int toDot = Math.max(dot1,dot2);
		int[] squareAndEdge= new int[]{-1,-1};
		
		//special case: right edge of rightmost squares
		if ((fromDot + 1) % 6 == 0) {
			squareAndEdge[0] = fromDot - (fromDot/5); 
			if ((toDot - fromDot) == 6)
				squareAndEdge[1] = 3;
		}
		
		//regular squares
		else if (fromDot < 29) {
			squareAndEdge[0] = fromDot - (fromDot/6);
			if ((toDot - fromDot) == 1) 
				squareAndEdge[1] = 0;
			else if ((toDot - fromDot) == 6)
				squareAndEdge[1] = 1;
		}
		//special case: bottom edge of bottom most squares
		else if (fromDot < 35) {
			squareAndEdge[0] = fromDot - 10;
			if ((toDot - fromDot) == 1) 
				squareAndEdge[1] = 2;
		}
		return squareAndEdge;

	}
	
	public void assignEdges() {
		for (int s=0; s < grid.length; s++) {
			grid[s] = new GridSquare();
			/* if there is square above, assign this squares
			 * top edge to that squares bottom egde
			 */
			if (s-5 >= 0) { 
				grid[s].edges[0] = grid[s-5].edges[2];
			}
			else 
				grid[s].edges[0] = new Edge();
			
			/* if you are not the rightmost square, assign 
			 * this squares left edge to the adjacent squares
			 * right edge
			 */
			if (s % 5 != 0) {
				grid[s].edges[1] = grid[s-1].edges[3];
			}
			else 
				grid[s].edges[1] = new Edge();
			
			/* assign bottom and right edges*/
			grid[s].edges[2] = new Edge();
			grid[s].edges[3] = new Edge();
			grid[s].setEdges();
		}
	}
}
