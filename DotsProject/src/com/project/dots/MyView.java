package com.project.dots;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.TextView;
import com.project.dots.R;

/* Handles the visual elements of Dots game */
public class MyView extends View implements Observer{
	private int viewDimension;
	private int padding; //distance between grid and view edge
	private int separation;
	private final static int DOT_DIMENSION = 6;
	private final static int SQUARE_DIMENSION = 5; // 2dots/square
	private Canvas canvas; 
	private Canvas tempCanvas;
	private Bitmap bitmap; // to draw on and load onto passed in canvas
	private Bitmap tempBitmap;
	private Paint paint; // handles drawing characteristics 
	private boolean init = false;
	private boolean shouldDraw = false;
	private boolean drawTempLine = false;
	static boolean onDevice = true;
	static boolean play = true;
	private int[] drawSpecs; //specifies which edges or squares to color
	private ScaleGestureDetector mScaleDetector;
	private float mScaleFactor = 1.f;
	private float x0,y0,x,y; //drag coords
	private LineManager lm;
	private String color;
	private int orangeScore = 0;
	private int limeScore = 0;
	private Context con; //context to display toast message
	private TextView orange;
	private TextView lime;
	static String myMoves = "";
	static String moveHistory = "";
	private final static int ORANGEEE = 0xFFFF9900;
	private final static int GREENNN = 0xFF57A300;
	private float[] lines;
	private Bitmap orangeBitmap;
	private Bitmap limeBitmap;
	private int graphicDim = 40;
	private BitmapFactory.Options options;
	private boolean loadingMove = false;
	private int startIdx = 0;
	private String [] moves;
	private DotsActivity myA;


	public MyView(Context context) {
		super(context);
	}
	
	public void setActivity(DotsActivity d) {
		myA = d;
	}
	public MyView(Context context, AttributeSet attr) {
		super(context,attr);
		con = context;
		mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());		
		lm = new LineManager();
		lm.addObserver(this); 
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        orangeBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.just_orange,options);
        orangeBitmap = Bitmap.createScaledBitmap(orangeBitmap,graphicDim,graphicDim,true);
		limeBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.just_lime,options);
		limeBitmap = Bitmap.createScaledBitmap(limeBitmap,graphicDim,graphicDim,true);
	}
	
	// handles touch events
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (play) {
	        mScaleDetector.onTouchEvent(ev);
	        final int action = ev.getAction();
	        switch (action & MotionEvent.ACTION_MASK) {
		        case MotionEvent.ACTION_DOWN: {
		            x0 = lm.findNearestDot(ev.getX());
		            y0 = lm.findNearestDot(ev.getY());
		            
		            break;
		        }
		        case MotionEvent.ACTION_MOVE: {
		        	x = ev.getX();
		        	y = ev.getY();
		        	tempCanvas.drawBitmap(bitmap, 
		        	          new Rect(0,0,bitmap.getWidth(),bitmap.getHeight()), 
		        	          new Rect(0,0,bitmap.getWidth(),bitmap.getHeight()), null);
		        	tempCanvas.drawLine(x0, y0, x, y, paint);
		        	if (color == "lime") {
		        		tempCanvas.drawBitmap(limeBitmap, x - (graphicDim/2), y - (graphicDim/2), paint);
		        	}
		        	else {
		        		tempCanvas.drawBitmap(orangeBitmap, x - (graphicDim/2), y - (graphicDim/2), paint);
		        	}
		        	drawTempLine = true;
		        	invalidate();
		        	break;
		        }
		        case MotionEvent.ACTION_UP: {
		        	x = ev.getX();
		        	y = ev.getY();
		        	drawTempLine = false;
		        	invalidate();
		        	lm.evalCoords(x0,y0,x,y);
		        	break;
		        	
		        }
	        }
		}
        return true;
	}

	public void setScoreViews(TextView b, TextView r) {
		lime = b;
		lime.setText("LIME: 0");
		orange = r;
		orange.setText("ORANGE: 0");
	}
	
	public void setGameType(boolean[] onDeviceAndPlay) {
		onDevice = onDeviceAndPlay[0];
		play = onDeviceAndPlay[1];
	}
	
	@Override
	protected void onDraw(Canvas c) {
        if (!init) {
        	initialize();
        	init = true;
        }
        if(shouldDraw) {
        	// draw line
        	float startX,startY,stopX,stopY;
        	startX = padding + (drawSpecs[0] % DOT_DIMENSION)*separation;
        	startY = padding + (drawSpecs[0]/ DOT_DIMENSION)*separation;
        	stopX = padding + (drawSpecs[1] % DOT_DIMENSION)*separation;
        	stopY = padding + (drawSpecs[1]/ DOT_DIMENSION)*separation;
        	canvas.drawLine(startX, startY, stopX, stopY, paint);
        	// draws the first square
        	if (drawSpecs[2] >= 0) {
        		paint.setAlpha(100);
        		float left = padding + (drawSpecs[2] % SQUARE_DIMENSION)*separation;
        		float top = padding + (drawSpecs[2]/ SQUARE_DIMENSION)*separation;
        		canvas.drawRect(left,top,left+separation,top+separation,paint);
        		if (color == "lime") {
        			limeScore++;
        		}
        		else 
        			orangeScore++;
        		
        		// draws the second square
        		if (drawSpecs[3] >= 0) {
            		left = padding + (drawSpecs[3]%5)*separation;
            		top = padding + (drawSpecs[3]/5)*separation;
            		canvas.drawRect(left,top,left+separation,top+separation,paint);
            		if (color == "lime") {
            			limeScore++;
            		}
            		else 
            			orangeScore++;
        		}
        		paint.setAlpha(255);
        		lime.setText("LIME: " + limeScore);
        		orange.setText("ORANGE: " + orangeScore);
        		if (limeScore >= 3 || orangeScore >= 3) {
        			addDialog();
        		}
        			
        	}
        	// if player did not complete square, turns switch
        	else  {
        		switchTurns();
        	}
        shouldDraw=false;
    }
        // add drawings to passed in canvas
        if (drawTempLine) {
	    	c.drawBitmap(tempBitmap, 
	    	   new Rect(0,0,tempBitmap.getWidth(),tempBitmap.getHeight()), 
	    	   new Rect(0,0,tempBitmap.getWidth(),tempBitmap.getHeight()), null);
        }
        else {
        	c.drawBitmap(bitmap, 
        		new Rect(0,0,bitmap.getWidth(),bitmap.getHeight()), 
        		new Rect(0,0,bitmap.getWidth(),bitmap.getHeight()), null);
        }
        //chains the move loading 
        if (loadingMove) {
        	loadNextMove();
        }
        /* TEST:: EXECUTED AFTER FIRST TURN
        if(!play) {
			loadMoves("1 2 2 3 3 4");
		}
		*/
    }
	private void switchTurns() {
		if (color == "lime") {
			color = "orange";
			paint.setColor(ORANGEEE);
		}
		else {
			color = "lime";
			paint.setColor(GREENNN);
		}
		canvas.drawLines(lines,paint);
		
		if (!onDevice) {
			play = !play;
		}
		
		if (!play && !loadingMove) {
			myA.updateGameObject(myMoves);
			myMoves = "";
			System.out.println(moveHistory);
		}
	}
	private void addDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(con);
		builder.setMessage("GAME OVER");
		if (limeScore > orangeScore)
			builder.setMessage("LIME WINS");
		else
			builder.setMessage("ORANGE WINS");
		
		myA.onGameOver(limeScore,orangeScore);
		myA.updateGameObject(myMoves);

		
		// Add the buttons
		builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		               Intent i = new Intent(con, GamePage.class);
		               i.putExtra(Constants.IntentUserName, myA.localUserName);
		               con.startActivity(i);
		           }
		       });
		builder.setNegativeButton("Restart", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
			        	if(onDevice)
			        	{
			        		   restart();
			        	}
			        	else
			        	{
			        			myA.onRematch();
			        	}
		        	
		           }
		       });

		AlertDialog dialog = builder.create();
		dialog.getWindow().setLayout(300,100);
		dialog.show();
	}
	
	
	private void restart() {
		limeScore = 0;
		orangeScore = 0;
		lime.setText("LIME: " + limeScore);
		orange.setText("ORANGE: " + orangeScore);
		init = false;
		lm.startNewGame();
		invalidate();
	}
	
	private void initialize() {
		
		viewDimension = getWidth();
		padding = 100;
		separation = (viewDimension - 2 * padding)/SQUARE_DIMENSION;
		padding = (viewDimension - SQUARE_DIMENSION*separation)/ 2; 
		lm.setPadding(padding);
		lm.setSeparation(separation);
		lines = new float[]{0,0,0,viewDimension,0,0,viewDimension,0,0,viewDimension,viewDimension,viewDimension,viewDimension,0,viewDimension,viewDimension};
		bitmap = Bitmap.createBitmap(viewDimension, viewDimension, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		tempBitmap = Bitmap.createBitmap(viewDimension, viewDimension, Bitmap.Config.ARGB_8888);
		tempCanvas = new Canvas(tempBitmap);
		paint.setColor(Color.WHITE);
		canvas.drawPaint(paint); // white background
		paint.setColor(Color.BLACK); //make dots
		Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.slice,options);
		bMap = Bitmap.createScaledBitmap(bMap,graphicDim,graphicDim,true);
		for (int i = 0; i < DOT_DIMENSION; i++) {
			for (int j = 0; j < DOT_DIMENSION; j++) {
				//canvas.drawCircle(padding+separation*i, padding+ separation*j, 20, paint);
				canvas.drawBitmap(bMap,padding+separation*i-(graphicDim/2), padding+ separation*j-(graphicDim/2), paint);
			}
		}
		//player 1 = blue
		paint.setColor(GREENNN);
		color = "lime";
		paint.setStrokeWidth(10);
		canvas.drawLines(lines,paint);
	}
	
	 private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));
            invalidate();
            return true;
        }
    }
		
	/*================================*
	 * LOADING & SAVING MOVES         *
	 * ===============================*
	 */
	private void loadNextMove() {
		startIdx += 2;
		if (startIdx < moves.length) {
			lm.checkAndFind(Integer.parseInt(moves[startIdx]),Integer.parseInt(moves[startIdx+1]));
		}
		else {
			loadingMove = false;
			startIdx = 0;
		}
	}	

	private void addToMyMoves(int x, int y) {
		myMoves +=  x + " " + y + " ";
	}
	
	public String grabMoves() {
		String m = myMoves;
		myMoves = "";
		return m;
	}
	
	public void restoreGame(String m, boolean p) {
		moveHistory = "";
		play = p;
		loadMoves(m);
	}
	
	//only load a move at a time
	public void loadMoves(String m) {
		int startIdx = 0;
		if (m.length() > 0) {
			moves = m.split(" ");
			loadingMove = true;
			lm.checkAndFind(Integer.parseInt(moves[startIdx]),Integer.parseInt(moves[startIdx+1]));
	
		}
	
	}
	
	private void addToMoveHistory(int d0, int d1) {
		moveHistory += d0 + " " + d1+ " ";
	}

	@Override
	public void update(Observable o, Object specs) {
		shouldDraw = true;
		drawSpecs = (int[]) specs;
		//dont load ops moves
		if (play && !loadingMove) {
			addToMyMoves(drawSpecs[0], drawSpecs[1]);
		}
		addToMoveHistory(drawSpecs[0], drawSpecs[1]);
		invalidate();
	}

}
