package com.project.dots;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;
import com.project.dots.R;

public class WifiDotsActivity extends ActionBarActivity {
	MyView vi;
    @Override
    public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       System.out.println("DOTS GAME");
       setContentView(R.layout.activity_dots);
       vi = (MyView)findViewById(R.id.draw);
       TextView title = (TextView)findViewById(R.id.title);
       TextView blue = (TextView)findViewById(R.id.blue);
       TextView red = (TextView)findViewById(R.id.red);
       vi.setScoreViews(blue,red);        
       
	   Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Font.ttf");
		
	   title.setTypeface(custom_font); 
	   blue.setTypeface(custom_font);
	   red.setTypeface(custom_font);
	   
	   
	   Intent intent = getIntent();
	   String method = intent.getStringExtra(android.content.Intent.EXTRA_TEXT);
	   if(method.equals("1"))
	   {
		   
	   }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       getMenuInflater().inflate(R.menu.main, menu);
       return true;
   }
}
