package com.project.dots;

/* Simple class that makes up the GridSquare class */
public class Edge {
	private boolean marked = false;
	
	public boolean isMarked() {
		return marked;
	}
	
	public void markDone() {
		marked = true;
	}
}	

