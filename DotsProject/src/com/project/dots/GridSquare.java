package com.project.dots;

/* GridSquare class is composed of 4 Edge Classes*/
public class GridSquare {
	Edge topEdge;//0
	Edge leftEdge;//1
	Edge bottomEdge;//2
	Edge rightEdge;//3
	Edge[] edges = new Edge[4]; 
	boolean colored = false;
	
	public void setEdges() {
		topEdge = edges[0];
		leftEdge = edges[1];
		rightEdge = edges[2];
		bottomEdge = edges[3];
	}

	public boolean isColored() {
		return colored;
	}
	
	public void setColored() {
		colored = true;
	}
	
	public boolean isComplete() {
		return topEdge.isMarked() && leftEdge.isMarked() && bottomEdge.isMarked() && rightEdge.isMarked();
	}
}
