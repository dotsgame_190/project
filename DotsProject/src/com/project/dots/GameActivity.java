package com.project.dots;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.shephertz.app42.paas.sdk.android.App42Response;
import com.shephertz.app42.paas.sdk.android.user.User;

public class GameActivity extends DotsActivity implements
		AsyncApp42ServiceApi.App42ServiceListener {

	private JSONObject gameObject;
	private int selectedCell = Constants.InvalidSelection;
	private String remoteUserName;
	private int localUserCellImageId;
	private AsyncApp42ServiceApi asyncService;
	private char localUserTile;
	private String currentState;
	private String nextTurn;
	private ProgressDialog progressDialog;
	private ImageButton selectedButton = null;
	private boolean fromNotification=false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.asyncService = AsyncApp42ServiceApi.instance();
		Intent intent = getIntent();
		
		try {
			
			if(GCMIntentService.isFromNotification){
				localUserName = getUserName();
				System.out.println("FROM NOTIF: " + localUserName);
				gameObject = new JSONObject(GCMIntentService.notificationMessage);
			}
			else{
			localUserName = intent.getStringExtra(Constants.IntentUserName);
			System.out.println("FROM INTENT: " + localUserName);
			gameObject = new JSONObject(
					intent.getStringExtra(Constants.IntentGameObject));
			}
			
			System.out.println(gameObject.toString());
			
			this.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onStart() {
		super.onStart();
		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				Constants.DisplayMessageAction));
		
	}
/*
 * This function allows to intialize the game 
 */
	private void initialize() {
		try {
						
			String u1Name = gameObject.getString(Constants.GameFirstUserKey);
			String u2Name = gameObject.getString(Constants.GameSecondUserKey);
			String winner = gameObject.getString(Constants.GameWinnerKey);
			currentState = gameObject.getString(Constants.GameStateKey);
			nextTurn = gameObject.getString(Constants.GameNextMoveKey);
			checkAndUpdateUserNAme(u1Name,u2Name);
			if (u1Name.equalsIgnoreCase(localUserName)) {
				//localUserCellImageId = R.drawable.cross_cell;
				localUserTile = Constants.BoardTileCross;
				remoteUserName = u2Name;
			} else {
				//localUserCellImageId = R.drawable.circle_cell;
				localUserTile = Constants.BoardTileCircle;
				remoteUserName = u1Name;
			}
			
			System.out.println("my username: " + localUserName);

			
			boolean play = false;
			
			// if my turn
			if(localUserName.equalsIgnoreCase(u1Name))
			{
				play = true;
			}
			else
			{
				play = false;
			}
			
			super.vi.restoreGame(gameObject.getString(Constants.MoveHistory),play);



		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void reinitialize(String completedMoves) {
		try {
						
			String u1Name = gameObject.getString(Constants.GameFirstUserKey);
			String u2Name = gameObject.getString(Constants.GameSecondUserKey);
			String winner = gameObject.getString(Constants.GameWinnerKey);
			currentState = gameObject.getString(Constants.GameStateKey);
			nextTurn = gameObject.getString(Constants.GameNextMoveKey);
			checkAndUpdateUserNAme(u1Name,u2Name);

			System.out.println("my username: " + localUserName);
			
			boolean play = false;
			
/*			// if my turn
			if(localUserName.equalsIgnoreCase(u1Name))
			{
				play = true;
			}
			else
			{
				play = false;
			}*/
			
			String moveHistory = gameObject.getString(Constants.GameBoardKey);
			String newMoves = moveHistory.replace(completedMoves,"");
			if(!newMoves.isEmpty() && newMoves.charAt(0) == ' ')
			{
				newMoves = newMoves.substring(1);
			}
			
			System.out.println("completed moves: " + completedMoves);
			System.out.println("all moves including new: " + moveHistory);
			System.out.println("just new moves: "  + newMoves);
			
			super.vi.loadMoves(newMoves);



		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	/*
	 * This function validate user whether from face-book or App42
	 */
	private void checkAndUpdateUserNAme(String u1Name,String u2Name){
		if(u1Name.equals(UserContext.myUserName)||
				u2Name.equals(UserContext.myUserName)){
			localUserName=UserContext.myUserName;
		}
	}
	

	/*
	 * This function allows to play game again if game is finished
	 */
	public void onRematchClicked(View view) {
		progressDialog = ProgressDialog.show(this, "", "creating game..");
		progressDialog.setCancelable(true);
		try {
			gameObject.put(Constants.GameFirstUserKey, localUserName);
			gameObject.put(Constants.GameSecondUserKey, remoteUserName);
			gameObject.put(Constants.GameStateKey,
					Constants.GameStateIdle);
			gameObject.put(Constants.GameBoardKey,
					Constants.GameIdleState);
			gameObject.put(Constants.GameWinnerKey, "");
			gameObject.put(Constants.GameNextMoveKey, localUserName);
			asyncService.updateGame(gameObject, this);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/*
	 * This function allows to handle event on submit button
	 */
	public void onSubmitClicked(View view) {
		try {
			String boardState = gameObject.getString(Constants.GameBoardKey);
			String prefix = "";
			if (selectedCell > 0) {
				prefix = boardState.substring(0, selectedCell);
			}

			String suffix = "";
			if (selectedCell < 8) {
				suffix = boardState.substring(selectedCell + 1);
			}

			String newBoardState = prefix + localUserTile + suffix;
			gameObject.put(Constants.GameBoardKey, newBoardState);
			gameObject.put(Constants.GameNextMoveKey, remoteUserName);
			if (isGameOver(newBoardState)) {
				gameObject.put(Constants.GameStateKey,
						Constants.GameStateFinished);
				gameObject.put(Constants.GameWinnerKey, localUserName);
			} else if (isBoardFull(newBoardState)) {
				gameObject.put(Constants.GameStateKey,
						Constants.GameStateFinished);
				gameObject.put(Constants.GameWinnerKey, "");
			} else {
				gameObject.put(Constants.GameStateKey,
						Constants.GameStateActive);
			}
			progressDialog = ProgressDialog.show(this, "", "sending move");
			asyncService.updateGame(gameObject, this);
			asyncService.pushMessage(gameObject, remoteUserName);
			System.out.println(gameObject.toString());
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void onSubmitClicked() {
		try {
			
			gameObject.put(Constants.GameNextMoveKey, remoteUserName);
			/*if (isGameOver(newBoardState)) {
				gameObject.put(Constants.GameStateKey,
						Constants.GameStateFinished);
				gameObject.put(Constants.GameWinnerKey, localUserName);
			} else if (isBoardFull(newBoardState)) {
				gameObject.put(Constants.GameStateKey,
						Constants.GameStateFinished);
				gameObject.put(Constants.GameWinnerKey, "");
			} else {
				gameObject.put(Constants.GameStateKey,
						Constants.GameStateActive);
			}*/
			progressDialog = ProgressDialog.show(this, "", "sending move BLA BLA BLA");
			asyncService.updateGame(gameObject, this);
			asyncService.pushMessage(gameObject, remoteUserName);
			System.out.println(gameObject.toString());
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/*
	 * This function checks the status of game
	 */
	private boolean isGameOver(String boardState) {
		// Check rows
		if (Utilities.areCharsEqual(boardState.charAt(0), boardState.charAt(1),
				boardState.charAt(2), localUserTile)
				|| Utilities.areCharsEqual(boardState.charAt(3),
						boardState.charAt(4), boardState.charAt(5),
						localUserTile)
				|| Utilities.areCharsEqual(boardState.charAt(6),
						boardState.charAt(7), boardState.charAt(8),
						localUserTile)) {
			return true;
		}

		// Check columns
		if (Utilities.areCharsEqual(boardState.charAt(0), boardState.charAt(3),
				boardState.charAt(6), localUserTile)
				|| Utilities.areCharsEqual(boardState.charAt(1),
						boardState.charAt(4), boardState.charAt(7),
						localUserTile)
				|| Utilities.areCharsEqual(boardState.charAt(2),
						boardState.charAt(5), boardState.charAt(8),
						localUserTile)) {
			return true;
		}

		// Check diagonals
		if (Utilities.areCharsEqual(boardState.charAt(0), boardState.charAt(4),
				boardState.charAt(8), localUserTile)
				|| Utilities.areCharsEqual(boardState.charAt(2),
						boardState.charAt(4), boardState.charAt(6),
						localUserTile)) {
			return true;
		}

		return false;
	}

	/*
	 * This function checks the board is full or not
	 */
	private boolean isBoardFull(String boardState) {
		for (int i = 0; i < 9; i++) {
			if (boardState.charAt(i) == Constants.BoardTileEmpty) {
				return false;
			}
		}

		return true;
	}



	@Override
	public void onUserCreated(User response) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserAuthenticated(App42Response response) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGetUserGamesList(ArrayList<JSONObject> arrayList) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreateGame(final JSONObject createdGameObject) {
		if (createdGameObject != null) {
			progressDialog.dismiss();
			GameActivity.this.gameObject = createdGameObject;
			GameActivity.this.initialize();
		}

	}

	/*
	 * This function retrives user name when notification receives
	 */
	private String getUserName() {
		SharedPreferences mPrefs = getSharedPreferences(
				MainActivity.class.getName(), MODE_PRIVATE);
		return mPrefs.getString(Constants.SharedPrefUname, null);
	}

	

	/*
	 * This method receives push notification and update the game
	 */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			
			
			try {
				
				localUserName = getUserName();
				
				
				
				JSONObject temp = new JSONObject(intent.getExtras().getString(
						Constants.NotificationMessage));
				
				if(gameObject.getString(Constants.GameIdKey).equalsIgnoreCase(temp.getString(Constants.GameIdKey)))
				{
					String completedMoves = gameObject.getString(Constants.GameBoardKey);
					localUserName = temp.getString(Constants.GameNextMoveKey);
					System.out.println("Recieved notification: " + temp.toString());
						System.out.println("Calling reinitialize");
						gameObject = temp;
						GameActivity.this.selectedButton = null;
						reinitialize(completedMoves);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	};

	@Override
    public void updateGameObject(String m) {
		System.out.println("GOT MOVES AT GAMEACTIVITY: " + m);
		try {
			String newBoard = gameObject.get(Constants.GameBoardKey) + m;
			gameObject.put(Constants.GameBoardKey, newBoard);
			gameObject.put(Constants.MoveHistory, newBoard);
			gameObject.put("sentfrom", localUserName);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//onUpdateGame(gameObject);
		onSubmitClicked();
    }
	
	@Override
	public void onGameOver(int u1Score, int u2Score) {
		try 
		{
			if(u1Score > u2Score)
			{
				gameObject.put(Constants.GameWinnerKey, gameObject.getString(Constants.GameFirstUserKey));
			}
			else
			{
				gameObject.put(Constants.GameWinnerKey, gameObject.getString(Constants.GameSecondUserKey));
			}
			
			gameObject.put(Constants.GameStateKey, Constants.GameStateFinished);
		} catch (JSONException e) {
				e.printStackTrace();
			}
		
    }

	@Override
	public void onRematch() {
		
		Intent i = new Intent(getApplicationContext(), UserHomeActivity.class);
		i.putExtra(Constants.IntentUserName,localUserName);
		i.putExtra("remoteUser",remoteUserName);
		startActivity(i);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mHandleMessageReceiver);
	}

	@Override
	public void onUpdateGame(final JSONObject updatedGameObject) {
		if (updatedGameObject != null) {
			progressDialog.dismiss();
			GameActivity.this.gameObject = updatedGameObject;
			//GameActivity.this.initialize();
		}
	}

}